class CreateQuotes < ActiveRecord::Migration[5.1]
  def change
    create_table :quotes do |t|
      t.references :search
      t.references :category
      t.string :icon_url
      t.string :external_id
      t.string :url
      t.text :body

      t.timestamps
    end
  end
end
