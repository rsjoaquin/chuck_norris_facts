class CreateSearches < ActiveRecord::Migration[5.1]
  def change
    create_table :searches do |t|
      t.string :email

      t.timestamps
    end

    create_table :quotes_searches, id: false do |t|
      t.belongs_to :quote, index: true
      t.belongs_to :search, index: true
    end
  end
end
