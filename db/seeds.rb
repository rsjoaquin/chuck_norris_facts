# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Category.create(name: "explicit")
Category.create(name: "dev")
Category.create(name: "movie")
Category.create(name: "food")
Category.create(name: "celebrity")
Category.create(name: "science")
Category.create(name: "sport")
Category.create(name: "political")
Category.create(name: "religion")
Category.create(name: "animal")
Category.create(name: "history")
Category.create(name: "music")
Category.create(name: "travel")
Category.create(name: "career")
Category.create(name: "money")
Category.create(name: "fashion")
Category.create(name: "empty")