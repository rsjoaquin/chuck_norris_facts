module Foundation
  class BasePresenter < SimpleDelegator
    include ActionView::Helpers::NumberHelper

    attr_reader :errors, :context, :options

    def initialize(data_obj, context, options = {}, errors = nil)
      @context = context
      @options = options
      @errors = errors || {}
      super(data_obj)
    end

    alias h context
    alias obj __getobj__

    def errors?
      errors.any?
    end

    def each_error
      errors.each do |_key, msg|
        yield I18n.t(msg)
      end
    end
  end
end
