class Quote < ApplicationRecord
  has_and_belongs_to_many :search
  belongs_to :category
end
