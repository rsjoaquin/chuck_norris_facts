class SearchesController < ApplicationController
  def new
    interactor = Searches::NewInteractor.new(self)
    interactor.call
    @presenter = interactor.build_presenter
  end

  def create
    interactor = Searches::CreateInteractor.new(self)
    interactor.call
    @presenter = interactor.build_presenter
    if interactor.success
      redirect_to interactor.search
    end
  end

  def show
    interactor = Searches::ShowInteractor.new(self)
    interactor.call
    @presenter = interactor.build_presenter
  end
end
