module Searches
  class ShowPresenter < ::Foundation::BasePresenter
    def quotes
      self[:quotes]
    end

    def email
      self[:email]
    end
  end
end
