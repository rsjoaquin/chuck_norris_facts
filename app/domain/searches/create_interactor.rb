module Searches
  class CreateInteractor < ::Foundation::BaseInteractor
    use_presenter_class Searches::NewPresenter
    
    def do_call
      @success = search.save
    end
    
    def search
      @_search ||= ::Search.new(quotes: fetch_quotes)
    end

    private

    def fetch_quotes
      if params["query"]
        query_fetcher
      elsif params["category"]
        category_fetcher
      elsif params["option"] == "random"
        random_fetcher
      end
    end

    def query_fetcher
      Services::Quote::Importer.new(
        ChuckNorris::Joke.new.query(params["query"])
      ).perform
    end

    def category_fetcher
      Services::Quote::Importer.new(
        [ChuckNorris::Joke.new.find_by_category(params["category"])]
      ).perform
    end

    def random_fetcher
      Services::Quote::Importer.new(
        [ChuckNorris::Joke.new.random]).perform
    end
  end
end
