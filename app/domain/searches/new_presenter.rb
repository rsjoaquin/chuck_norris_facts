module Searches
  class NewPresenter < ::Foundation::BasePresenter
    def categories
      self[:categories]
    end
  end
end
