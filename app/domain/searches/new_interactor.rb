module Searches
  class NewInteractor < ::Foundation::BaseInteractor
    use_presenter_class Searches::NewPresenter

    def do_call
      response_params[:categories] = categories
    end

    private

    def categories
      Category.all.pluck(:name).uniq
    end
  end
end
