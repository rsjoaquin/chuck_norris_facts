module Searches
  class ShowInteractor < ::Foundation::BaseInteractor
    use_presenter_class Searches::ShowPresenter
    
    def do_call
      response_params[:quotes] = quotes
      response_params[:email] = search.email
    end

    private

    def search
      ::Search.find(params[:id])
    end

    def quotes
      search.quotes.includes(:category).map do |quote|
        { url: quote.url,
          image_url: quote.icon_url,
          body: quote.body,
          category: quote.category.name
        }
      end
    end
  end
end
