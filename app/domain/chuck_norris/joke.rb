module ChuckNorris
  class Joke
    attr_reader :api

    def initialize(api=ChuckNorris::API)
      @api = api.new
    end

    def random
      HTTParty.get(api.random)
    end

    def find_by_category(category)
      HTTParty.get(api.find_by_category(category))
    end

    def query(query)
      HTTParty.get(api.query(query))["result"]
    end
  end
end