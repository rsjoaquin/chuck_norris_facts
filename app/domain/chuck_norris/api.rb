module ChuckNorris
  class API
    def random
      url_base + "/random"
    end

    def find_by_category(category)
      url_base + "/random?category=#{category}"
    end

    def query(query)
      url_base + "/search?query=#{query}"
    end

    def categories
      url_base + "/categories"
    end

    private

    def url_base
      config_file["url_base"]
    end

    def config_file
      @_file ||= YAML.load_file(Rails.root.join("app", "domain", "chuck_norris", "config.yml"))
    end
  end
end