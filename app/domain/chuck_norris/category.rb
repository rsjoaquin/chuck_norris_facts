module ChuckNorris
  class Category
    attr_reader :api

    def initialize(api=ChuckNorris::API)
      @api = api.new
    end

    def all
      HTTParty.get(api.categories)
    end
  end
end