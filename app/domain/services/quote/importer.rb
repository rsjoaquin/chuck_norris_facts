module Services
  module Quote
    class Importer
      attr_reader :raw_quotes

      def initialize(raw_quotes)
        @raw_quotes = raw_quotes
      end

      def perform
        raw_quotes.map do |raw_quote|
          ::Quote.where(external_id: raw_quote[:id]).first_or_create do |quote|
            quote.category = Services::Category::Importer.new(raw_quote["category"]&.first).perform
            quote.icon_url = raw_quote["icon_url"]
            quote.external_id = raw_quote["id"]
            quote.url = raw_quote["url"]
            quote.body = raw_quote["value"]
          end
        end
      end
    end
  end
end
