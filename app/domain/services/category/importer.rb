module Services
  module Category
    class Importer
      attr_reader :name

      def initialize(name)
        @name = name
      end

      def perform
        ::Category.where(name: name).first_or_create do |category|
          category.name = name || "empty"
        end
      end
    end
  end
end